﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssert;

namespace RomanNumerals.Tests
{
    [TestClass]
    public class BusinessLogicTests
    {
        private BusinessLogic logic;

        [TestInitialize]
        public void Setup()
        {
            logic = new BusinessLogic();    
        }

        [TestMethod]
        public void Should_convert_1_to_I()
        {
            logic.ConvertNumberToRomanNumeral(1).ShouldBeEqualTo("I");
        }

        [TestMethod]
        public void Should_convert_5_to_V()
        {
            logic.ConvertNumberToRomanNumeral(5).ShouldBeEqualTo("V");
        }

        [TestMethod]
        public void Should_convert_10_to_X()
        {
            logic.ConvertNumberToRomanNumeral(10).ShouldBeEqualTo("X");
        }

        [TestMethod]
        public void Should_convert_50_to_L()
        {
            logic.ConvertNumberToRomanNumeral(50).ShouldBeEqualTo("L");
        }

        [TestMethod]
        public void Should_convert_100_to_C()
        {
            logic.ConvertNumberToRomanNumeral(100).ShouldBeEqualTo("C");
        }

        [TestMethod]
        public void Should_convert_500_to_D()
        {
            logic.ConvertNumberToRomanNumeral(500).ShouldBeEqualTo("D");
        }

        [TestMethod]
        public void Should_convert_1000_to_M()
        {
            logic.ConvertNumberToRomanNumeral(1000).ShouldBeEqualTo("M");
        }

        [TestMethod]
        public void Should_convert_2_to_II()
        {
            logic.ConvertNumberToRomanNumeral(2).ShouldBeEqualTo("II");
        }

        [TestMethod]
        public void Should_convert_6_to_VI()
        {
            logic.ConvertNumberToRomanNumeral(6).ShouldBeEqualTo("VI");
        }

        [TestMethod]
        public void Should_convert_26_to_XXVI()
        {
            logic.ConvertNumberToRomanNumeral(26).ShouldBeEqualTo("XXVI");
        }

        [TestMethod]
        public void Should_convert_1888_to_MDCCCLXXXVIII()
        {
            logic.ConvertNumberToRomanNumeral(1888).ShouldBeEqualTo("MDCCCLXXXVIII");
        }

        [TestMethod]
        public void Should_convert_4_to_IV()
        {
            logic.ConvertNumberToRomanNumeral(4).ShouldBeEqualTo("IV");
        }

        [TestMethod]
        public void Should_convert_9_to_IX()
        {
            logic.ConvertNumberToRomanNumeral(9).ShouldBeEqualTo("IX");
        }

        [TestMethod]
        public void Should_convert_49_to_XLIX()
        {
            logic.ConvertNumberToRomanNumeral(49).ShouldBeEqualTo("XLIX");
        }

        [TestMethod]
        public void Should_convert_455_to_CDLV()
        {
            logic.ConvertNumberToRomanNumeral(455).ShouldBeEqualTo("CDLV");
        }

        [TestMethod]
        public void Should_convert_974_to_CMLXXIV()
        {
            logic.ConvertNumberToRomanNumeral(974).ShouldBeEqualTo("CMLXXIV");
        }
    }
}
