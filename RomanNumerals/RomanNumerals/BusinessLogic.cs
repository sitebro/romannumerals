﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RomanNumerals
{
    public class BusinessLogic
    {
        public string ConvertNumberToRomanNumeral(int numberToConvert)
        {
            return GetRomanNumeral(numberToConvert);
        }

        private string GetRomanNumeral(int numberToConvert)
        {
            string result = string.Empty;
            int multiple, remainder;
            remainder = numberToConvert;

            multiple = remainder / 1000;
            remainder = remainder % 1000;
            for (int i = 1; i <= multiple; i++)
            {
                result += "M";
            }
            if (remainder >= 900)
            {
                result += "CM";
                remainder -= 900;
            }
            multiple = remainder / 500;
            if (multiple > 0)
            {
                result += "D";
            }
            remainder = remainder % 500;
            if (remainder >= 400)
            {
                result += "CD";
                remainder -= 400;
            }
            multiple = remainder / 100;
            remainder = remainder % 100;
            for (int i = 1; i <= multiple; i++)
            {
                result += "C";
            }
            multiple = remainder / 50;
            if (multiple > 0)
            {
                result += "L";
            }
            remainder = remainder % 50;
            if (remainder >= 40)
            {
                result += "XL";
                remainder -= 40;
            }
            multiple = remainder / 10;
            remainder = remainder%10;
            for (int i = 1; i <= multiple; i++)
            {
                result += "X";
            }
            if (remainder == 9)
            {
                result += "IX";
                remainder -= 9;
            }
            multiple = remainder/5;
            if (multiple > 0)
            {
                result += "V";
            }
            remainder = remainder%5;
            multiple = remainder/1;
            if (multiple == 4)
            {
                result += "IV";
                multiple -= 4;
            }
            for (int i = 1; i <= multiple; i++)
            {
                result += "I";
            }
            return result;
        }
    }
}